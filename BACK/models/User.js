const mongoose = require('mongoose')

const schema = mongoose.Schema({
    firstName: { type: String, required: false },
    lastName: { type: String, required: false },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    recepies: [{
        name: String,
        category: String,
        tags: [{ name: String}],
        ingredients: [{ name: String, quantity: String}],
        origin: String,
        description: String,
        steps: [{ description: String }]      
    }]
})

module.exports = mongoose.model('User', schema)