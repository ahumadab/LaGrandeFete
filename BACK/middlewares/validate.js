const jwt = require("jsonwebtoken");
const SECRET_KEY = process.env.TOKEN_SECRET;

exports.checkJWT = async (req, res, next) => {
    let token = req.cookies.token;
    // console.log(token);
    if (token) {
      jwt.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
          return res.status(401).json("token_not_valid");
        } else {
          req.decoded = decoded;
          // console.log(decoded._id);
          const expiresIn = 30 * 60 * 1000;
          const newToken = jwt.sign(
            {
              userId: decoded._id,
            },
            SECRET_KEY,
            {
              expiresIn: expiresIn,
            }
          );
          // console.log(newToken);
          res.cookie("token", newToken, {
            maxAge: 30 * 60 * 1000, // 1 hour
            httpOnly: true,
          });
          next();
        }
      });
    } else {
      return res.status(200).json("token_required");
    }
  };