const express = require("express");
const mongoose = require('mongoose')
const cors = require("cors");
require("dotenv").config();
const PORT = process.env.PORT || 5000; // if port not available switches to port 5000
const url = process.env.URL;

// Routes
const authRoutes = require("./routes/authRoutes");
const userRoutes = require("./routes/userRoutes");
// const authRoutes = require("./routes/authRoutes");

mongoose
  .connect(url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    const app = express()
    app.use(cors())

    app.use(express.json())

    app.use('/api/user', userRoutes)
    app.use('/api', authRoutes)

    app.listen(PORT, () => {
      console.log(`Server has started at http://localhost:${PORT}`)
    })
  })
  .catch(err => {
    console.error('Database connection error', err)
  })